//
//  ViewController.swift
//  Postcard
//
//  Created by Daniel Smith on 16/06/2015.
//  Copyright (c) 2015 Kuma Kuma Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var enterNameTextField: UITextField!
    @IBOutlet weak var enterMessageTextField: UITextField!
    @IBOutlet weak var mailButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func sendMailButtonPressed(sender: UIButton) {
        
        // adding a comment to test git commit
        
        nameLabel.hidden = false
        nameLabel.text = "Dear \(enterNameTextField.text)"
        nameLabel.textColor = UIColor.blueColor()
        
        messageLabel.hidden = false
        messageLabel.text = enterMessageTextField.text
        
        messageLabel.textColor = UIColor.redColor()
        
        enterMessageTextField.text = ""
        enterNameTextField.text = ""
        
        if enterMessageTextField.isFirstResponder() {
            enterMessageTextField.resignFirstResponder()
        } else if enterNameTextField.isFirstResponder() {
            enterNameTextField.resignFirstResponder()
        }
        
        mailButton.setTitle("Mail Sent", forState: UIControlState.Normal)
    }

}

